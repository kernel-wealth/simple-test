﻿using System;
using System.Threading;

namespace SimpleTest
{
    // ------------
    // Interface is supplied by 3rd party
    // ------------

    public record Portfolio
    {
        public decimal MarketValue { get; init; }
        public decimal BookValue { get; init; }
    }

    public record Profile
    {
        public string Email { get; init; }
        public string Firstname { get; init; }
        public string Lastname { get; init; }
        public string PortfolioId { get; init; }
        public int UserId { get; init; }
    }

    public interface IThirdParty
    {
        Profile GetProfile(string userId);
        Portfolio GetPortfolio(string portfolioId);
    }

    // ------------
    // TASK: create a class that implements the interface below. 
    // ------------

    public record KernelPortfolio
    {
        public decimal Value { get; init; }
    }

    public interface IKernel
    {
        KernelPortfolio GetMarketValue(string userId);
    }
}
